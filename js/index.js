$(function () {
    $('[data-toggle="tooltip"]').tooltip();
    $('[data-toggle="popover"]').popover();
    $('.carousel').carousel({
      interval: 2000
    });


    $('#suscribeteModal').on('show.bs.modal', function (e) {
      console.log('El modal se esta mostrando');

      $('#suscribeteBtn').removeClass('btn-modal');
      $('#suscribeteBtn').addClass('btn-primary');
      $('#suscribeteBtn').prop('disabled', true);
    });

    $('#suscribeteModal').on('shown.bs.modal', function (e) {
      console.log('El modal se mostro');

    });

    $('#suscribeteModal').on('hide.bs.modal', function (e) {
      console.log('El modal se esta ocultando');

    });

    $('#suscribeteModal').on('hidden.bs.modal', function (e) {
      console.log('El modal se oculto');
      $('#suscribeteBtn').removeClass('btn-primary');
      $('#suscribeteBtn').addClass('btn-modal');
      $('#suscribeteBtn').prop('disabled', false);
    });


  });